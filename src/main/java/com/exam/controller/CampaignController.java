package com.exam.controller;

import com.exam.model.Campaign;
import com.exam.repository.CampaignRepository;
import com.exam.service.ICampaignService;
import com.exam.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.swing.text.DateFormatter;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/api")
public class CampaignController {

	private static final Logger logger = LoggerFactory.getLogger(CampaignController.class);

	private final ICampaignService campaignService; //Service which will do all data retrieval/manipulation work

	@Autowired
	public CampaignController(ICampaignService campaignService) {
		this.campaignService = campaignService;
	}

	@PostMapping("/сampaign")
	public ResponseEntity createCampaign(
			@RequestParam String name,
			@RequestParam String startDate,
			@RequestParam String category,
			@RequestParam String bid) {

		Campaign campaign = Campaign.builder()
				.bid(new BigDecimal(bid))
				.name(name)
				.category(category)
				.startDate(LocalDate.parse(startDate))
				.build();

		logger.info("Creating Campaign : {}", campaign);

		if (campaign.getId() != 0
				&& CampaignRepository.getCampaigns().stream()
				.mapToLong(Campaign::getId)
				.anyMatch(id -> campaign.getId() == 1)) {
			logger.error("Unable to create. A Campaign with name {} already exist", campaign.getId());
			return new ResponseEntity<>(new CustomErrorType("Unable to create. A Campaign with name " +
					campaign.getId() + " already exist."),HttpStatus.CONFLICT);
		}
		Campaign result = campaignService.save(campaign);


		return new ResponseEntity<>( result, HttpStatus.CREATED);
	}

}