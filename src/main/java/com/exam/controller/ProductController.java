package com.exam.controller;

import com.exam.model.Campaign;
import com.exam.model.Product;
import com.exam.service.IAdsService;
import com.exam.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ProductController {

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    private final IAdsService adsService;

    public ProductController(IAdsService adsService) {
        this.adsService = adsService;
    }

    @GetMapping("/ads/{category}")
    public ResponseEntity getAds(@PathVariable String category ) {

        logger.info("Getting ads : {}", category);

        if (StringUtils.isEmpty(category)) {
            logger.error("Category is empty");
            return new ResponseEntity<>(new CustomErrorType("Category is empty"), HttpStatus.BAD_REQUEST);
        }
        Product result = adsService.getAdsByCategory(category);

        return new ResponseEntity<>( result, HttpStatus.CREATED);
    }

}
