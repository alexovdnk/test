package com.exam.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

//@Entity
public class Campaign {
//    @Id
    private long id;
    private String name;
    private String category;
    private BigDecimal bid;
    // made without time for simplicity
    private LocalDate startDate;

//    @ManyToMany
//    @JoinTable(name = "campaigns_products",
//            inverseJoinColumns = @JoinColumn(name = "id_product", referencedColumnName = "id"),
//            joinColumns = @JoinColumn(name = "id_campaign", referencedColumnName = "id")
//    )
    List<Product> products;
}
