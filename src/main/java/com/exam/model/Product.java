package com.exam.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
//@Entity
public class Product {

//    @Id
    private String serialNumber;
    private String title;

    //Also we can make an entity for categories relations
    private String category;

    private BigDecimal price;

//    @ManyToMany(mappedBy = "products")
    private List<Campaign> campaign;

}
