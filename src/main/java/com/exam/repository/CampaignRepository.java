package com.exam.repository;

import com.exam.model.Campaign;
import com.exam.util.GeneratorUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class CampaignRepository {

    private static List<Campaign> campaigns = new ArrayList<>();

    public static List<Campaign> getCampaigns() {

        for (int i = 0; i < 2; i++) {
            Campaign product = Campaign.builder()
                    .bid(new BigDecimal("20")).category("MilkProduct" + i)
                    .id((long) i)
                    .startDate(LocalDate.from(LocalDate.now().minus(10 + i,  ChronoUnit.DAYS)))
                    .build();
            campaigns.add(product);
        }
        return campaigns;
    }

    public static boolean save(Campaign campaign) {
        campaign.setId(GeneratorUtils.generateId());
        return campaigns.add(campaign);
    }
}
