package com.exam.repository;

import com.exam.model.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProductRepository {

    public static List<Product> getProducts() {
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Product product =  Product.builder()
                    .title("Milk" + i)
                    .category("MilkProduct" + i)
                    .price(new BigDecimal("2").multiply(BigDecimal.valueOf(i)))
                    .serialNumber(UUID.randomUUID().toString())
                    .build();
            products.add(product);
        }
        return products;
    }

}
