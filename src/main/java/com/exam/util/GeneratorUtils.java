package com.exam.util;

import java.util.concurrent.ThreadLocalRandom;

public class GeneratorUtils {

    // It's not secure in real project i'll delegate this to db side
    public static long generateId() {
        int min = 1;
        int max = 1000;
        return ThreadLocalRandom.current().nextLong(min, max + 1);
    }



}
