package com.exam.service;

import com.exam.model.Campaign;

public interface ICampaignService {
    Campaign save(Campaign campaign);
}
