package com.exam.service;

import com.exam.model.Campaign;
import com.exam.model.Product;
import com.exam.repository.CampaignRepository;
import com.exam.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class AdsServiceImpl implements IAdsService {
    @Override
    public Product getAdsByCategory(String category) {
        List<Campaign> campaigns = CampaignRepository.getCampaigns().stream()
                .filter(c -> c.getCategory().equals(category) && isActive(c)).collect(toList());
        if(campaigns.size() < 1) {
            return ProductRepository.getProducts().stream().max(Comparator.comparing(Product::getPrice)).orElse(null);
        }
        return null;
    }

    private boolean isActive(Campaign campaign) {
        return campaign.getStartDate().isAfter(LocalDate.now().minus(10, ChronoUnit.DAYS));
    }
}
