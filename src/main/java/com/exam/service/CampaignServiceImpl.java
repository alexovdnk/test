package com.exam.service;

import com.exam.model.Campaign;
import com.exam.model.Product;
import com.exam.repository.CampaignRepository;
import com.exam.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class CampaignServiceImpl implements ICampaignService {
    @Override
    public Campaign save(Campaign campaign) {
        //Business logic here
        List<Product> products = ProductRepository
                .getProducts()
                .stream()
                .filter(p -> p.getCategory().equals(campaign.getCategory()))
                .collect(toList());
        campaign.setProducts(products);
        CampaignRepository.save(campaign);
        return campaign;
    }
}
