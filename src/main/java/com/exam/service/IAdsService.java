package com.exam.service;

import com.exam.model.Product;

public interface IAdsService {

    Product getAdsByCategory(String category);

}
